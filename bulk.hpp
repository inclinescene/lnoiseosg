// bulk.hpp: Header to be precompiled for general use.
// Yes, this is overkill for many use cases :) .

// Copyright (c) 2017 cxw/Incline.  CC-BY-SA 3.0.  In any derivative work,
// mention or link to https://bitbucket.org/inclinescene/public and
// http://devwrench.com.

//#pragma once  // This gives a warning, so don't include it.

#ifndef _BULK_HPP_
#define _BULK_HPP_

#define _USE_MATH_DEFINES
    // Or you don't get M_PI from math.h

// C and C++ standard library headers
#include <algorithm>
#include <cassert>
#include <climits>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <map>
#include <math.h>
#include <memory>
#include <ostream>
#include <sstream>
#include <stdarg.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>
#include <wchar.h>

// OpenThreads headers
#include <OpenThreads/Block>
#include <OpenThreads/Mutex>
#include <OpenThreads/ScopedLock>
#include <OpenThreads/Thread>
#include <OpenThreads/WorkQueue>

// OpenSceneGraph headers
#include <osg/Object>
#include <osg/Node>
#include <osg/Array>
#include <osg/Billboard>
#include <osg/BoundingBox>
#include <osg/Callback>
#include <osg/CoordinateSystemNode>
#include <osg/Drawable>
#include <osg/FrameStamp>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Group>
#include <osg/LightModel>
#include <osg/Material>
#include <osg/Math>
#include <osg/MatrixTransform>
#include <osg/Matrixf>
#include <osg/Matrixd>
#include <osg/Notify>
#include <osg/Point>
#include <osg/PositionAttitudeTransform>
#include <osg/Program>
#include <osg/Quat>
#include <osg/ScriptEngine>
#include <osg/ScriptUtils>
#include <osg/Shader>
#include <osg/Shape>
#include <osg/ShapeDrawable>
#include <osg/Switch>
#include <osg/Timer>
#include <osg/Types>
#include <osg/Uniform>
#include <osg/ValueObject>
#include <osg/Vec3d>
#include <osg/Vec3f>
#include <osg/Vec4d>
#include <osg/Vec4f>
#include <osgDB/FileUtils>
#include <osgDB/InputStream>
#include <osgDB/ObjectWrapper>
#include <osgDB/OutputStream>
#include <osgDB/ReadFile>
#include <osgDB/Serializer>
#include <osgDB/WriteFile>
#include <osgGA/AnimationPathManipulator>
#include <osgGA/Device>
#include <osgGA/DriveManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/GUIActionAdapter>
#include <osgGA/GUIEventAdapter>
#include <osgGA/KeySwitchMatrixManipulator>
#include <osgGA/SphericalManipulator>
#include <osgGA/StateSetManipulator>
#include <osgGA/TerrainManipulator>
#include <osgGA/TrackballManipulator>
#include <osgSim/MultiSwitch>
#include <osgText/Font>
#include <osgText/Text3D>
#include <osgText/Text>
#include <osgUtil/Optimizer>
#include <osgViewer/GraphicsWindow>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgViewer/config/SingleScreen>
#include <osgViewer/config/SingleWindow>

#endif //_BULK_HPP_
// vi: set ts=4 sts=4 sw=4 et ai ff=dos ft=cpp: //
