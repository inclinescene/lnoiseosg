// lnoiseosg main.cpp: Test of linenoise with OpenSceneGraph
// Uses code modified from osgviewer and from
// https://github.com/dpapavas/luaprompt.git .
// Original copyrights below.

// Copyright (c) 2017 cxw/Incline.  CC-BY-SA 3.0.  In any derivative work,
// mention or link to https://bitbucket.org/inclinescene/public and
// http://devwrench.com.

// Headers //////////////////////////////////////////////////////////// {{{1
#include "bulk.hpp"

#ifdef CONCRETE_LUA
// Makefile adds -Isrc/osgPlugins
#include <lua/LuaScriptEngine.h>
#endif // CONCRETE_LUA

#include "common.h"
#include <osg/Depth>

// For prompting
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
    // *** POSIX threads, not Win32 ***
#include <setjmp.h>

#include <sys/ioctl.h>
#include <sys/select.h>

#include <glob.h>

//#include <readline/readline.h>
//#include <readline/history.h>

#define LINENOISE_INTERNALS
#include "linenoise/linenoise.h"

// Environment variables
#define LOL_RUN_ENV_VAR ("LOL_RUN")
    // initial script to run.  The script is actually run from livecoding.lua,
    // but we check it here for camera-manipulator control.
#define LOL_CAMON_ENV_VAR ("LOL_CAMON")
    // If set, always enable the camera manipulator at startup.
#define LOL_WARN_ENV_VAR ("LOL_VERBOSE_LUA")
    // If set, leave the notify level unchanged while executing Lua code.
    // Otherwise, only WARN-level messages are shown.

using namespace osg;
using namespace std;
using namespace OpenThreads;

// }}}1
// Prompting ////////////////////////////////////////////////////////// {{{1

// Helpers // {{{2
#define print_output(...) fprintf (stdout, __VA_ARGS__), fflush(stdout)
#define print_error(...) fprintf (stderr, __VA_ARGS__), fflush(stderr)

#define COLOR(i) (colorize ? colors[i] : "")

static string logfile(".livecoding.history");

static const int colorize = 1;
static const char *colors[] = {"\033[0m",
                               "\033[0;31m",
                               "\033[1;31m",
                               "\033[0;32m",
                               "\033[1;32m",
                               "\033[0;33m",
                               "\033[1;33m",
                               "\033[1m",
                               "\033[22m"};

#if 0
void handle_interrupt(int) {
    siglongjmp(before_readline, 1);
}

static void display_matches (char **matches, int num_matches, int max_length)
{
    print_output ("%s", COLOR(7));
    rl_display_match_list (matches, num_matches, max_length);
    print_output ("%s", COLOR(0));
    rl_on_new_line ();
}

static char *keyword_completions (const char *text, int state)
{
    static const char **c, *keywords[] = {
        "and", "break", "do", "else", "elseif", "end", "false", "for",
        "function", "if", "in", "local", "nil", "not", "or",
        "repeat", "return", "then", "true", "until", "while", NULL
    };

    int s, t;

    if (state == 0) {
        c = keywords - 1;
    }

    /* Loop through the list of keywords and return the ones that
     * match. */

    for (c += 1 ; *c ; c += 1) {
        s = strlen (*c);
        t = strlen(text);

        if (s >= t && !strncmp (*c, text, t)) {
            return strdup (*c);
        }
    }

    return NULL;
}


static char *generator (const char *text, int state)
{
    static int which;
    char *match = NULL;

    if (state == 0) {
        which = 0;
    }

    /* Try to complete a keyword. */

    if (which == 0) {
        if ((match = keyword_completions (text, state))) {
            return match;
        }
        which += 1;
        state = 0;
    }

    /* Try to complete a module name. */

    if (which == 1) {
#if 0
        if ((match = module_completions (text, state))) {
            return match;
        }
#endif
        which += 1;
        state = 0;
    }

    /* Try to complete a table access. */

    if (which == 2) {
#if 0
        look_up_metatable = 0;
        if ((match = table_key_completions (text, state))) {
            return match;
        }
#endif
        which += 1;
        state = 0;
    }

    /* Try to complete a metatable access. */

    if (which == 3) {
#if 0
        look_up_metatable = 1;
        if ((match = table_key_completions (text, state))) {
            return match;
        }
#endif
        which += 1;
        state = 0;
    }

    /* Try to complete a file name. */

    if (which == 4) {
        if (text[0] == '\'' || text[0] == '"') {
            match = rl_filename_completion_function (text + 1, state);

            if (match) {
                struct stat s;
                int n;

                n = strlen (match);
                stat(match, &s);

                /* If a match was produced, add the quote
                 * characters. */

                match = (char *)realloc (match, n + 3);
                memmove (match + 1, match, n);
                match[0] = text[0];

                /* If the file's a directory, add a trailing backslash
                 * and suppress the space, otherwise add the closing
                 * quote. */

                if (S_ISDIR(s.st_mode)) {
                    match[n + 1] = '/';

                    rl_completion_suppress_append = 1;
                } else {
                    match[n + 1] = text[0];
                }

                match[n + 2] = '\0';
            }
        }
    }

    return match;
}
#endif

static void finish ()
{
    /* Save the command history on exit. */

    if (!logfile.empty()) {
        linenoiseHistorySave(logfile.c_str());
    }
}

// }}}2
// CLI thread // {{{2

typedef WorkQueue<string> StringQ;
typedef WorkQueue< ref_ptr<ScriptError> > ResponseQ;

/// Thread that receives input from the user and sends it to the main thread
/// for execution.  Adapted from luaprompt.
class CLIThread: public Thread
{
public:
    /// Let Lua warnings through
    static bool show_warnings;

private:
    // Exit support
    int fd_request_exit_, fd_exit_requested_;

    // Prompt strings
    static const char *single;
    static const char *multi;

    /// Commands from CLIThread to the main thread
    StringQ& from_me_;

    /// Responses from the main thread to the CLIThread
    ResponseQ& to_me_;

    /// Prompts used, a la luaprompt.
    string prompts[2][2];

    void exec_string(const string& cmd, ref_ptr<ScriptError>& response)
    {
        // Suppress NOTICE messages so we don't get an error printout
        // on an incomplete line we are going to handle anyway.
        NotifySeverity severity = getNotifyLevel();
        if(!show_warnings && (severity >= NotifySeverity::NOTICE)) {
            // Higher numbers mean more messages --- if we are getting
            // NOTICE messages or more than that, drop the level to
            // WARN until the command is done.
            setNotifyLevel(NotifySeverity::WARN);
        }

        from_me_.put(cmd);    // Execute
        to_me_.get(response);       // Block for a response

        // Restore the severity level
        setNotifyLevel(severity);
    } //exec_string

    enum Resp { OK, Error, Incomplete };

    /// The last error message we got.
    string lastErr_;

    Resp handle_response(ref_ptr<ScriptError>& response)
    {
        if(response.get() == 0) {
            OSG_NOTICE << "NULL ScriptError object" << endl;
            return OK;      // Discard any partial comment

        } else if(response->getErrorMessage().find("<eof>") != string::npos) {
            //partial command - leave cmd untouched.
            return Incomplete;

        } else if(response->getErrorMessage() == "no error") {
            return OK;

        } else {    // Error.  It's up to the caller to report it if desired.
            //cout << "Response to -" << cmd << "-: " << endl;
            //cout << response->getErrorMessage() << endl;
            lastErr_ = response->getErrorMessage();
            //cout << "FN  " << response->getFileName() << endl;
            //cout << "Ln  " << response->getLineNumber() << endl;
            //cout << "TB  " << response->getTracebackMessage() << endl;
            cout << endl;

            return Error;
        }
    } //handle_response

    char *buffer;   // TODO move this into run()

    // Linenoise customization
    static CLIThread *me_;

    // Read bytes of the next character based on *me_.
    static size_t cliReadCode(int fd, char *buf, size_t buf_len, int *cp) {
        if(!buf || !cp || buf_len<1) {
            errno = EINVAL;
            return -1;
        }

        while(1) {

            // Wait for data on the exit-request pipe or the terminal
            int maxfd = me_->fd_exit_requested_;
            if(fd > maxfd) maxfd = fd;

            fd_set rfds;
            FD_ZERO(&rfds);
            FD_SET(fd, &rfds);
            FD_SET(me_->fd_exit_requested_, &rfds);

            int selret = select(maxfd+1, &rfds, NULL, NULL, NULL);
            if(selret == -1) {
                int olderrno = errno;
                //cerr << "Select error" << endl;
                errno = olderrno;   // in case cout resets errno
                return -1;
            } else if(!selret) {
                //cerr << "No data" << endl;
                continue;   // keep waiting
            }

            // If we get here, there is data.
            if(FD_ISSET(me_->fd_exit_requested_, &rfds)) {
                char exitbuf[4] = {0};
                int nread = read(me_->fd_exit_requested_, exitbuf, 1);
                if(nread == -1) {
                    int olderrno = errno;
                    //cerr << "pipefd read error " << errno << endl;
                    errno = olderrno;
                    return -1;
                }
                //cout << "Exiting - got " << buf << endl;
                errno = 0;  // no error
                return 0;   // EOF
            } //endif exiting

            if(FD_ISSET(fd, &rfds)) {
                int nread = read(fd, buf, 1);
                if(nread == -1) {
                    int olderrno = errno;
                    //cerr << "stdin read error " << errno << endl;
                    errno = olderrno;
                    return -1;
                }
                //fprintf(stderr, "Got %02x\n", (int)buf[0]);

                if(nread == 1) *cp = buf[0];
                return nread;
            } //endif stdin bytes

        } //while

    } //cliReadCode()

public:
    /// Set by other threads to indicate this thread should exit
    bool should_exit;

    /// Set by this thread to indicate it is already exiting in response
    /// to a user "exit" command
    bool is_exiting;

    void requestExit()
    {
        char c = 'q';   // anything nonzero
        write(fd_request_exit_, (const void *)&c, 1);
    } //requestExit()

    CLIThread(StringQ& cmds, ResponseQ& responses)
    : Thread()
    , from_me_(cmds)
    , to_me_(responses)
    , buffer(NULL)
    , should_exit(false)
    , is_exiting(false)
    {
        OSG_NOTICE << "CLIThread created" << endl;

        // Exit pipes
        int fds[2];
        if(pipe(fds) != 0) {
            throw runtime_error("Could not create pipe");
        }
        fd_request_exit_ = fds[1];      //write
        fd_exit_requested_ = fds[0];    //read

        /* Plain, uncolored prompts. */

        prompts[0][0] = single;
        prompts[0][1] = multi;

        /* Colored prompts. */

        char buf[256];  // Fixed buffer size = bad!
        sprintf (buf, "%s%s%s",
                 COLOR(6), single, COLOR(0));
        prompts[1][0] = buf;
        sprintf (buf, "%s%s%s",
                 COLOR(6), multi, COLOR(0));
        prompts[1][1] = buf;

#if 0
        rl_readline_name = "livecoding";
        rl_basic_word_break_characters = " \t\n`@$><=;|&{(";
        rl_completion_entry_function = generator;
        rl_completion_display_matches_hook = display_matches;

#endif
        /* Load the command history if there is one. */
        if (!logfile.empty()) {
            linenoiseHistoryLoad(logfile.c_str());
        }

        cout << endl << "*** lnoiseosg main ***" << endl;
        cout << "by cxw/Incline 2017--2018" << endl << endl;
        cout << "Built-in commands are exit, camon, camoff, help()." << endl;

        linenoiseSetMultiLine(true);
        if(enableRawMode(STDIN_FILENO) == -1) {
            throw runtime_error("Couldn't enable raw mode");
        }

        me_ = this;
        linenoiseSetEncodingFunctions(
            defaultPrevCharLen,
            defaultNextCharLen,
            cliReadCode
        );

    } //ctor

    virtual ~CLIThread() {
        OSG_NOTICE << "CLIThread destructor called in thread " <<
            hex << pthread_self() << endl;
        disableRawMode(STDIN_FILENO);
        // Save the updated command history
        if (!logfile.empty()) {
            linenoiseHistorySave(logfile.c_str());
        }
    }

    virtual void run()   //luap_enter(lua_State *L)
    {

        // CAUTION: uncaught exceptions, segvs, &c. in this thread won't
        // be reported by the Cygwin runtime.
        // TODO wrap this whole thing in a block like main+innards below.

        OSG_NOTICE << "CLIThread run() called in thread " <<
            hex << pthread_self() << endl;

        int incomplete = 0, s = 0, t = 0;

        ref_ptr<ScriptError> response;

        while (!should_exit) {

            // Get a line
            char *line = linenoise_R(incomplete ?
                           prompts[colorize][1].c_str() :
                           prompts[colorize][0].c_str());
            int theerrno = errno;

            if(!line && (theerrno == EAGAIN)) {    // Ctrl-C
                incomplete = 0;     // Discard any command in progress
                s=0;
                t=0;
                if(buffer) *buffer='\0';
                continue;

            } else if(!line) {                  // Ctrl-D
                break;
            }

            if (*line == '\0') {    //empty line?
                free(line);
                continue;
            }

            /* Add/copy the line to the buffer. */

            if (incomplete) {
                s += strlen (line) + 1;

                if (s > t) {
                    buffer = (char *)realloc (buffer, s + 1);
                    t = s;
                }

                strcat (buffer, "\n");
                strcat (buffer, line);
            } else {
                s = strlen (line);

                if (s > t) {
                    buffer = (char *)realloc (buffer, s + 1);
                    t = s;
                }

                strcpy (buffer, line);
            }

            string sbuffer(buffer);
            Resp r;
            string to_exec;

            if(sbuffer=="exit") {
                from_me_.put(sbuffer);
                break;
            }

            //Special commands
            if(sbuffer=="camon" || sbuffer=="camoff") {
                to_exec = sbuffer;

            } else if(incomplete) {         // Don't try to execute incomplete
                r = Incomplete;             // statements as expressions.
                to_exec = sbuffer;

            } else {
                // Try to execute fresh lines as an expression to be printed.

                // TODO? use a "return" instead of a "print" and get the
                // output parameter from the script?
                to_exec = "print_r( (";
                    // We already loaded print_r in init()
                to_exec += sbuffer;
                to_exec += ") );";  //Try to let the interpreter know this is it
            }

            exec_string(to_exec, response);
            r = handle_response(response);

            if(r == OK) {
                incomplete = 0;

            } else if( (!incomplete) && (r == Incomplete) ) {
                // It wasn't incomplete, so we tried to execute it as an
                // expression, and we got an Incomplete.  This is odd.
                OSG_NOTICE << "Incomplete in an expression?!!??"  << endl
                    << " => `" << sbuffer << '`' << endl;

            } else if( incomplete && (r == Incomplete) ) {
                (void)0;    // It was incomplete, and it still is - do nothing.

            } else {    // A syntax error.  Maybe it's a statement?
                /* Try to execute the line as-is. */

                exec_string(sbuffer, response);
                r = handle_response(response);

                incomplete = 0;

                if(r == Incomplete) {
                    incomplete = 1;
                } else if(r == Error) {
                    cout << lastErr_ << endl;
                    lastErr_ = "";
                }
            }

            /* Add the line to the history if non-empty. */

            if (!incomplete) {
                linenoiseHistoryAdd(buffer);
            }

            free (line);
        } //while(!should_exit)

        is_exiting = true;
        from_me_.put("exit");   // Tell the main thread to exit
        print_output ("\n");
        OSG_NOTICE << "CLIThread run() exiting from thread " << pthread_self()
                    << endl;
    }
}; //CLIThread

const char *CLIThread::single = "Cmd? (or 'exit') > ";
const char *CLIThread::multi =  "                 > ";
bool CLIThread::show_warnings = false;
CLIThread *CLIThread::me_ = 0;

// }}}2
// }}}1
// Lua runner ///////////////////////////////////////////////////////// {{{1

/// RunLuaHandler telling main() to turn on or off the camera manipulator
static volatile enum { NoCamCmd, TurnOn, TurnOff } CamManipCmd = NoCamCmd;
    // TODO figure out a way to permit Lua to control the manipulator

/// Run a Lua command, if one has been sent over the pipe.  This is an event
/// callback and not an update callback because we need access to the event
/// queue in order to trigger an exit.
class RunLuaHandler: public osgGA::GUIEventHandler {
private:
    StringQ& to_me_;
    ResponseQ& from_me_;

    /// A response we will reuse for all successful commands
    ref_ptr<ScriptError> no_error_;

public:
    /// A script with useful C++ interface routines.  Public so the
    /// main loop can access it.
    ref_ptr<Script> cpp_interface;
    ref_ptr<ScriptEngine> engine;

    RunLuaHandler(ScriptEngine *theengine, StringQ& cmds, ResponseQ& responses)
    : to_me_(cmds)
    , from_me_(responses)
    , no_error_(new ScriptError("no error"))
    , cpp_interface(0)
    , engine(theengine)
    {
        init();
    }

    /// Load Lua functions we always want available
    void init()
    {
        Parameters inparms, outparms;

        // Load print_r
        inparms.clear(); outparms.clear();
        cout << "Ignore the warning about \"getType\" - it's from the "
            "`return print_r` at the end of print_r.lua." << endl;
        if(!execScriptFile("print_r.lua", engine, inparms, outparms)) {
            OSG_NOTICE << "Could not load print_r" << endl;
        }

        // Load cpp_interface
        cpp_interface = osgDB::readRefScriptFile("cpp-interface.lua");
        inparms.clear(); outparms.clear();
        if(!engine->run(cpp_interface, "", inparms, outparms)) {
            throw runtime_error("Could not load cpp_interface");
        }
    } //init

    virtual bool handle(const osgGA::GUIEventAdapter& ea,
                        osgGA::GUIActionAdapter& aa,
                        osg::Object*,
                        osg::NodeVisitor* nv)
    {
        if(ea.getEventType() != osgGA::GUIEventAdapter::FRAME) {
            return false;
        }

        // Once per frame, run Lua code.

        { // Call the Lua perFrameEvent() function
            auto fs = nv->getFrameStamp();
            auto sim_time = fs->getSimulationTime();

            Parameters inparms, outparms;
            inparms.push_back(new DoubleValueObject(sim_time));

            engine->run(cpp_interface, "perFrameEvent", inparms, outparms);
                // Ignore return value - one error message per frame
                // would be ugly.
        }

        // Check for a Lua command.
        string cmd;

        if(!to_me_.getIfAvailable(cmd)) {
            return false;
        }

        // Special-purpose commands
        if(cmd=="exit") {
            auto view = aa.asView();
            osgViewer::View* vview = dynamic_cast<osgViewer::View*>(view);
            if(!vview) {
                OSG_NOTICE << "Could not get osgViewer::View" << endl;
            } else {
                ref_ptr<osgGA::GUIEventAdapter> event(
                                                new osgGA::GUIEventAdapter);
                event->setEventType(osgGA::GUIEventAdapter::QUIT_APPLICATION);

                auto queue = vview->getEventQueue();
                queue->addEvent(event);
                    // EventQueue holds ref_ptr's, so it's OK that our
                    // ref_ptr goes out of scope here.
            }
            from_me_.put(no_error_);    // let the CLI thread keep going

        } else if(cmd=="camon" && CamManipCmd == NoCamCmd) {
            cout << "Requesting manipulator on" << endl;
            CamManipCmd = TurnOn;
            from_me_.put(no_error_);

        } else if(cmd=="camoff" && CamManipCmd == NoCamCmd) {
            cout << "Requesting manipulator off" << endl;
            CamManipCmd = TurnOff;
            from_me_.put(no_error_);

        } else {    // General commands
            ref_ptr<Script> script(new Script("lua",cmd));
            Parameters inparms, outparms;

            if(!engine->run(script, "", inparms, outparms)) {
                //cout << "Could not run command -" << cmd << "-" << endl;
                from_me_.put(engine->getLastSavedError());
            } else {
                from_me_.put(no_error_);
            }
        }

        return false;   // false => other handlers get the FRAME event, too.
    } //handle()

}; //RunLuaHandler

bool setLuaGlobal(RunLuaHandler *lua, const std::string& name, Object *val)
{
    if(lua->cpp_interface) {
        Parameters inparms, outparms;
        inparms.clear(); outparms.clear();
        inparms.push_back(new StringValueObject(name));
        inparms.push_back(val);
        if(!lua->engine->run(lua->cpp_interface, "loadGlobal", inparms, outparms))
        {
            cout << "Could not set global " << name << endl;
            return false;
        }
    } else {
        return false;
    }
    return true;

}

// }}}1
// Uniforms /////////////////////////////////////////////////////////// {{{1

ref_ptr<Uniform> setupResolutionUniform(ref_ptr<osgViewer::Viewer> viewer)
{
    ref_ptr<Uniform> iResolution(
            new osg::Uniform(osg::Uniform::FLOAT_VEC2, "iResolution"));

    // Set the initial value, since the resize handler doesn't fire on startup.
    {
        ref_ptr<const Viewport> vp(viewer->getCamera()->getViewport());
        // Sometimes this is NULL in full-screen mode.
        // As a result, full-screen mode is not currently supported.
        // TODO FIXME
        if(!vp) {
            throw runtime_error(
                "Can't run in full-screen mode --- set OSG_WINDOW before running"
            );
        }
        Vec2f v(vp->width(), vp->height());
        iResolution->set(v);
    }

    // Add the uniforms before adding the program, since that's what osgshaders does
    viewer->getSceneData()->getOrCreateStateSet()->addUniform(iResolution);
    viewer->addEventHandler(new ResolutionResizeHandler(iResolution));

    return iResolution;
} //setupResolutionUniform

class UpdateTimeCallback : public Callback {
private:
    ref_ptr<Uniform> u_;
public:
    UpdateTimeCallback(Uniform *u): u_(u) {}
    virtual bool run(Object* object, Object* data)
    {
        do {
            auto nv = data->asNodeVisitor();
            if(!nv) break;
            auto fs = nv->getFrameStamp();
            if(!fs) break;
            u_->set((float)fs->getSimulationTime());
        } while(0);
        return Callback::run(object, data);
    } //run
}; //UpdateTimeCallback

// }}}1
// Version checking /////////////////////////////////////////////////// {{{1
// From https://vicrucann.github.io/tutorials/osg-version-opengl/

class TestSupportOperation : public osg::GraphicsOperation
{
    //typedef const GLubyte*  (*GLGETSTRING)(GLenum);

public:
    TestSupportOperation()
        : osg::Referenced(true)
        , osg::GraphicsOperation("TestSupportOperation", false)
        , m_supported(true)
        , m_errorMsg()
        , m_version(0.0)
    {}

    virtual void operator() (osg::GraphicsContext* gc)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> lock(m_mutex);
        osg::GLExtensions* gl2ext = gc->getState()->get<osg::GLExtensions>();

        if( gl2ext ){

            if( !gl2ext->isGlslSupported )
            {
                m_supported = false;
                m_errorMsg = "ERROR: GLSL not supported by OpenGL driver.";
            }
            else
            {
                m_version = gl2ext->glVersion;
                glslversion = gl2ext->glslLanguageVersion;

                // Ideas from http://forum.openscenegraph.org/viewtopic.php?t=7004
                // and https://stackoverflow.com/a/126553/2877364 by
                // https://stackoverflow.com/users/6799/nearaz
                //GLGETSTRING gl_get_string = (GLGETSTRING)getGLExtensionFuncPtr("glGetString");
                //if(gl_get_string) {
                //gc->makeCurrent();
                // For some reason these return NULL, even with the makeCurrent call first.  TODO figure out later.
                //    vendor = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
                //    renderer = reinterpret_cast<const char*>(glGetString(GL_RENDERER));
                //    extensions = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
                //    version = reinterpret_cast<const char*>(glGetString(GL_VERSION));
                //}
            }
        } else {
            m_supported = false;
            m_errorMsg = "ERROR: GLSL not supported.";
        }
    }

    OpenThreads::Mutex  m_mutex;
    bool                m_supported;
    std::string         m_errorMsg;
    float               m_version;
    float               glslversion;
    //std::string         vendor, renderer, extensions, version;
}; //class TestSupportOperation

void printOpenGLVersionAndRealize(osgViewer::Viewer* viewer)
{
    osg::ref_ptr<TestSupportOperation> tester = new TestSupportOperation;
    viewer->setRealizeOperation(tester.get());
    viewer->realize();

    if (tester->m_supported) {
        std::cout << "=======================================" << endl;
        std::cout << "GLVersion=   " << tester->m_version << std::endl;
        std::cout << "GLSL Version=" << tester->glslversion << std::endl;
        //std::cout << "Vendor=      " << tester->vendor << std::endl;
        //std::cout << "Renderer=    " << tester->renderer << std::endl;
        //std::cout << "Extensions=  " << tester->extensions << std::endl;
        //std::cout << "Version=     " << tester->version << std::endl;
        std::cout << "=======================================" << endl;
    } else {
        std::cout << tester->m_errorMsg << std::endl;
    }
}

// }}}1
// Main /////////////////////////////////////////////////////////////// {{{1

int innards() //(int argc, char** argv)
{
    // Check the environment
    {
        const char *s = getenv(LOL_WARN_ENV_VAR);
        if(s && *s) {
            CLIThread::show_warnings = true;
            cout << "Showing Lua output at notify level " <<
                getNotifyLevel() << endl;
        }

        // If there's a LOL_RUN, assume it will handle the camera.
        CamManipCmd =
            (getenv(LOL_RUN_ENV_VAR) && !getenv(LOL_CAMON_ENV_VAR))
                ? TurnOff : NoCamCmd;
            // NoCamCmd because we are currently always loading the
            // manipulator on startup.
    }

    // Start up threading
    Thread::Init();

    cout << "main() running in thread " << pthread_self() << endl;

    StringQ command_queue;
    ResponseQ response_queue;

#ifdef CONCRETE_LUA
        // Create the LuaScriptEngine.
        // readRefObject always returns ref_ptr<Object>, so you need an
        // extra dynamic_cast.  readRefFile<T>() returns ref_ptr<T> directly.
        cout << "Creating concrete LuaScriptEngine" << endl;
        ref_ptr<lua::LuaScriptEngine> lse =
            osgDB::readRefFile<lua::LuaScriptEngine>("ScriptEngine.lua");
#else
        cout << "Creating abstract ScriptEngine" << endl;
        ref_ptr<ScriptEngine> lse =
            osgDB::readRefFile<ScriptEngine>("ScriptEngine.lua");
#endif //CONCRETE_LUA else

        if(!lse) {
            cout << "Could not create script engine with readRefFile" << endl;
            return 1;
        }

    CLIThread clithread(command_queue, response_queue);
    clithread.start();

    // Viewer in main thread per http://forum.openscenegraph.org/viewtopic.php?t=9520
    int retval = 1;     // error exit if we break; set to 0 at the end of the do
    do {    // The scope of the viewer, in a do block so we can use `break`
            // to take us to the cleanup code.
        ref_ptr<osgViewer::Viewer> viewer(new osgViewer::Viewer());
        viewer->getCamera()->setName("main camera");
        viewer->getCamera()->setClearColor(osg::Vec4(0,0,0,1));
        viewer->setQuitEventSetsDone(true);

        ref_ptr<osg::Group> sceneData(new osg::Group());
        viewer->setSceneData(sceneData);

        printOpenGLVersionAndRealize(viewer);

        // add the help handler
        viewer->addEventHandler(new osgViewer::HelpHandler());

        // add the state manipulator
        viewer->addEventHandler(
            new osgGA::StateSetManipulator(
                                viewer->getCamera()->getOrCreateStateSet()) );

        // add the thread model handler
        viewer->addEventHandler(new osgViewer::ThreadingHandler);

        // add the window size toggle handler
        viewer->addEventHandler(new osgViewer::WindowSizeHandler);

        // add the stats handler
        viewer->addEventHandler(new osgViewer::StatsHandler);

        ref_ptr<RunLuaHandler> lua =
                new RunLuaHandler(lse.get(), command_queue, response_queue);
        viewer->addEventHandler(lua);

        // Give the Lua script a reference to the camera
        setLuaGlobal(lua, "CAM", viewer->getCamera());

        // Give the Lua script the root node
        setLuaGlobal(lua, "ROOT", sceneData);

        // Add the iResolution global now that we have a window.
        auto iResolution = setupResolutionUniform(viewer);

        ref_ptr<Uniform> iGlobalTime(new Uniform("iGlobalTime", 0.0f));
        sceneData->getOrCreateStateSet()->addUniform(iGlobalTime);
        sceneData->addUpdateCallback(new UpdateTimeCallback(iGlobalTime));

        // Load the initial geometry in the script engine we already have,
        // rather than using readRefNodeFile (which creates a new engine).
        Parameters inparms, outparms;

        if(!execScriptFile("livecoding.lua", lse, inparms, outparms)) {
            cout << "Could not run livecoding.lua" << endl;
            clithread.should_exit = true;
            command_queue.put("");
            break;  // out to the cleanup code
        }

        cout << "Got " << outparms.size() << " outputs from livecoding.lua" << endl;
        ref_ptr<Node> model(makeNodeFromParams(outparms));
        sceneData->addChild(model);

        // Run the main loop
        // TODO: Set the initial camera position as a manipulator would
        //       - make the camera manipulator togglable from Lua
        const double minFrameTime = 1.0 / 60.0;   // in sec

        osg::ref_ptr<osgGA::TrackballManipulator> manip(
                new osgGA::TrackballManipulator);

        // HACK: for now, use the manipulator to set the initial camera
        if(CamManipCmd == NoCamCmd) cout << "camera manipulator on" << endl;
        viewer->setCameraManipulator(manip);
        viewer->frame();

        while(!viewer->done())
        {
            osg::Timer_t startFrameTick = osg::Timer::instance()->tick();

            // Toggle the camera manipulator
            if(CamManipCmd == TurnOn && viewer->getCameraManipulator() == 0) {
                cout << "camera manipulator on" << endl;
                // These three lines don't work to make the manipulator
                // take on the current position - not sure why not.  TODO.
                //manip = new osgGA::TrackballManipulator(0);
                //manip->setAutoComputeHomePosition(false);
                //manip->setByMatrix(viewer->getCamera()->getViewMatrix());
                viewer->setCameraManipulator(manip);
                CamManipCmd = NoCamCmd;     // Let the Lua handler have the var back

            } else if(CamManipCmd == TurnOn) {
                cout << "camera manipulator already on" << endl;
                CamManipCmd = NoCamCmd;

            } else if(CamManipCmd == TurnOff) {
                cout << "camera manipulator off" << endl;
                viewer->setCameraManipulator(0);
                CamManipCmd = NoCamCmd;
            }

            // Run the frame
            viewer->frame();

            osg::Timer_t endFrameTick = osg::Timer::instance()->tick();
            double frameTime = osg::Timer::instance()->delta_s(startFrameTick, endFrameTick);
            if (frameTime < minFrameTime) OpenThreads::Thread::microSleep(static_cast<unsigned int>(1000000.0*(minFrameTime-frameTime)));
        } //while(!done())

        retval = 0;     // successful exit
    } while(0);
      //end scope - destroy the viewer here so there isn't an inert window
      //sitting on screen

    // Clean up the CLI thread
    clithread.should_exit = true;
    cout << "Requesting exit" << endl;
    clithread.requestExit();
    cout << "Waiting for CLI thread" << endl;
    clithread.join();

    cout << endl;

    finish();

    cout << "leaving innards()" << endl;
    return retval;
    // clithread dtor is called as we leave innards.  It is called from
    // the context of the main thread.
} //innards

int main(int , char ** )
{   // Because the standard C++ library I'm using on cygwin terminates
    // silently on an uncaught exception, rather than printing a message.
    try {
        int retval = innards();
        cout << "leaving main()" << endl;
        return retval;
    } catch(exception& e) {
        cerr << "Uncaught exception: " << e.what() << endl;
        return 1;
    } catch(...) {
        cerr << "There was an uncaught custom exception.\n";
        return 2;
    }
} //main

// }}}1
// Original copyrights //////////////////////////////////////////////// {{{1

// osgviewer
/* -*-c++-*- OpenSceneGraph - Copyright (C) 1998-2010 Robert Osfield
 *
 * This application is open source and may be redistributed and/or modified
 * freely and without restriction, both in commercial and non commercial applications,
 * as long as this copyright notice is maintained.
 *
 * This application is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

// luaprompt
/* Copyright (C) 2012-2015 Papavasileiou Dimitris
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// }}}1
// vi: set ts=4 sts=4 sw=4 et ai fo=crql fdm=marker: //
