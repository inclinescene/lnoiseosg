# Makefile for lnoiseosg on cygwin.  Derived from the cmake-generated Makefile
# for examples/osggeometry.
#
# Copyright (c) 2017 cxw/Incline.  CC-BY-SA 3.0.  In any derivative work,
# mention or link to https://bitbucket.org/inclinescene/public and
# http://devwrench.com.

# Set default build configuration, if one is not specified in the environment
CC ?= gcc
CXX ?= g++

OSG_BUILD_NAME ?= gcc

# Shorthand for build configuration
N=$(OSG_BUILD_NAME)
D=build-$(N)
	# dest dir
I=../osg/$(D)
	# build include dir

# Default rule
all: $(D)/main.exe
	@-mkdir -p "$(D)"

# No builtin rules, per https://stackoverflow.com/a/4126617/2877364 by
# https://stackoverflow.com/users/280577/john-marshall
.SUFFIXES:

# Clean whenever the Makefile changes.
# Thanks to https://stackoverflow.com/a/3892826/2877364 by
# https://stackoverflow.com/users/128940/beta
# A file in "$(D)" is used because that directory is not tracked by git.
-include $(D)/z_dummy

$(D)/z_dummy: Makefile
	@echo Makefile changed - rebuilding
	@-mkdir -p "$(D)"
	@touch $@
	@$(MAKE) -s clean

CXXFLAGS += -DNOMINMAX -D_GNU_SOURCE -D_XOPEN_SOURCE=500 \
		-std=c++11 \
		-I../osg/include -I/usr/include/w32api \
		-I${I}/include \
		-I../respawn/common \
		-isystem/usr/include/w32api -DWIN32_LEAN_AND_MEAN \
		-DOSG_HACK_RENAME_READERWRITER_INTERNALS -DZIP_STD -Wall \
		-Wparentheses -Wno-long-long -Wno-import -pedantic \
		-Wreturn-type -Wmissing-braces -Wunknown-pragmas \
		-Wunused -Wextra -Wshadow \
		-Winvalid-pch \
		-g -D_DEBUG -O0

#LDFLAGS for cyg_native OSG, i.e., OpenGL from w32api rather than X11
LDFLAGS += -L${I}/bin -L${I}/lib \
		-Wl,--enable-auto-import \
		-Wl,--major-image-version,0,--minor-image-version,0  \
		-losgViewerd -losgGAd \
		-losgDBd -losgTextd \
		-lz -losgUtild -losgd -lOpenThreadsd \
		-Wl,-Bstatic -lm -ldl -Wl,-Bdynamic -lgdi32 \
		/usr/lib/w32api/libopengl32.a

# How to use precompiled headers

BULK=$(D)/bulk.hpp
	# what to call it in a #include / -include

ifeq (${N},gcc)
BULKOUT=$(BULK).gch
	# what it actually is
USE_PCH = -include $(BULK)
else ifeq (${N},clang)
BULKOUT=$(BULK).pch
#USE_PCH = -include-pch $(BULKOUT)
USE_PCH=
endif

# Compiler-specific flags used for building both pch and o.
ifeq ($(N),gcc)
CXXFLAGS += -Wmaybe-uninitialized
else ifeq ($(N),clang)
CXXFLAGS += -Wuninitialized -Wno-overloaded-virtual \
		-m64 -mcmodel=large
endif

### Real targets ###

# Thanks to http://stackoverflow.com/a/38058698/2877364 by
# http://stackoverflow.com/users/50617/employed-russian
# for explanation --- explicitly listing libfoo.a always links it; using
# -lfoo links it only if necessary.
# To generate a map file, add -Wl,-Map,$(D)/content.map to the link command.

# Note: the ### is a burst marker so I can find the start of the error
# messages more easily.

$(D)/main.o: main.cpp $(BULKOUT)
	#####################################################################
	$(CXX) \
		${CFLAGS} ${CXXFLAGS} $(USE_PCH) \
		-o $@ -c $<

$(D)/main.exe: $(D)/main.o $(D)/common.o $(D)/linenoise.o
	$(CXX)  \
		$^ -o $@ \
		${LDFLAGS} -lreadline -lpthread -lrt

$(D)/common.o: common/common.cpp common/common.h $(BULKOUT)
	$(CXX) \
		${CFLAGS} ${CXXFLAGS} $(USE_PCH) \
		-o $@ -c $<

$(D)/linenoise.o: linenoise/linenoise.c
	$(CXX) \
		${CFLAGS} ${CXXFLAGS} \
		-o $@ -c $<

# === Precompiled headers ===
ifeq (${N},gcc)

$(BULKOUT): bulk.hpp
	@echo Pre-compiling $(BULKOUT)
	$(CXX) \
		${CFLAGS} ${CXXFLAGS} \
		-o $@ -c $<

else ifeq (${N},clang)

$(BULKOUT): bulk.hpp
	@echo Pre-compiling $(BULKOUT)
	$(CXX) \
		${CFLAGS} ${CXXFLAGS} \
		-x c++-header $< \
		-Xclang -emit-pch \
		-Xclang -relocatable-pch \
		-o $@

# For clang, we need -Xclang before -emit-pch so the driver won't barf.
# Not sure if we need -relocatable-pch.

endif

clean:
	-rm $(D)/common.o
	-rm $(D)/main.o
	-rm $(D)/main.exe
	-rm $(BULKOUT)

distclean: clean
	- rm $(D)/z_dummy

# vi: set fo=crql: #
