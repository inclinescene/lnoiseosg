// Modified by cxw42@github from
// The Art of C++ / Sequences
// Copyright (c) 2015 Daniel Frey
//
// This version, as the original, licensed MIT.  Specifically:
//
//Copyright (c) 2015-2017 Daniel Frey and cxw42
//Permission is hereby granted, free of charge, to any person
//obtaining a copy of this software and associated documentation files
//(the "Software"), to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify, merge,
//publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be
//included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef MAKE_INTEGER_SEQUENCE_HPP_
#define MAKE_INTEGER_SEQUENCE_HPP_

#include <cstddef>
#include <utility>
#include <type_traits>

#include "integer_sequence.hpp"

namespace Incline
{
  namespace seq
  {

#ifdef TAOCPP_USE_STD_MAKE_INTEGER_SEQUENCE

    using std::make_integer_sequence;
    using std::make_index_sequence;
    using std::index_sequence_for;

#else

    // idea from https://stackoverflow.com/a/13073076
    // by https://stackoverflow.com/users/500104/xeo

    namespace impl
    {
      template< typename, std::size_t, bool >
      struct double_up;

      template< typename T, T... Ns, std::size_t N >
      struct double_up< integer_sequence< T, Ns... >, N, false >
      {
          typedef integer_sequence< T, Ns..., (N + Ns)... > type;
          // using type = integer_sequence< T, Ns..., (N + Ns)... >;
      };

      template< typename T, T... Ns, std::size_t N >
      struct double_up< integer_sequence< T, Ns... >, N, true >
      {
          typedef integer_sequence< T, Ns..., (N + Ns)..., 2 * N > type;
          // using type = integer_sequence< T, Ns..., (N + Ns)..., 2 * N >;
      };

      template< typename T, T N, typename = void >
      struct generate_sequence;

      template< typename T, T N >
      struct generate_sequence_t {
          typedef typename generate_sequence< T, N >::type type;
          //using generate_sequence_t = typename generate_sequence< T, N >::type;
      };

      template< typename T, T N, typename >
      struct generate_sequence
        : double_up< typename generate_sequence_t< T, N / 2 >::type, N / 2, N % 2 == 1 >
      {};

      template< typename T, T N >
      struct generate_sequence< T, N, typename std::enable_if< ( N == 0 ) >::type >
      {
          typedef integer_sequence<T> type;
          // using type = integer_sequence< T >;
      };

      template< typename T, T N >
      struct generate_sequence< T, N, typename std::enable_if< ( N == 1 ) >::type >
      {
          typedef integer_sequence<T, 0> type;
          // using type = integer_sequence< T, 0 >;
      };
    }

    template< typename T, T N >
    struct c1001_fix_maybe_mis {
        typedef typename impl::generate_sequence_t<T, N>::type type;
    };
    //template< typename T, T N >
    //using make_integer_sequence = typename c1001_fix_maybe_mis<T, N>::type; //impl::generate_sequence_t< T, N >;
        // C1001 internal compiler error on the line above.

    template< typename T, T N >
    struct make_integer_sequence {
        typedef typename c1001_fix_maybe_mis<T, N>::type type;
    };

    template< std::size_t N >
    using make_index_sequence = typename make_integer_sequence< std::size_t, N >::type;

    template< typename... Ts >
    using index_sequence_for = make_index_sequence< sizeof...( Ts ) >;

#endif

  }
}

#endif // MAKE_INTEGER_SEQUENCE_HPP_
// vi: set ts=2 sts=2 sw=2 et ai: //
