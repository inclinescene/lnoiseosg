// Modified by cxw42@github from
// The Art of C++ / Sequences
// Copyright (c) 2015 Daniel Frey
//
// This version, as the original, licensed MIT.  Specifically:
//
//Copyright (c) 2015-2017 Daniel Frey and cxw42
//Permission is hereby granted, free of charge, to any person
//obtaining a copy of this software and associated documentation files
//(the "Software"), to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify, merge,
//publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be
//included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef INTEGER_SEQUENCE_HPP_
#define INTEGER_SEQUENCE_HPP_

#include <cstddef>
#include <utility>

namespace Incline
{
  namespace seq
  {

#ifdef TAOCPP_USE_STD_INTEGER_SEQUENCE

    using std::integer_sequence;
    using std::index_sequence;

#else

    template< typename T, T... Ns >
    struct integer_sequence
    {
      using value_type = T;
      static const std::size_t nelems = sizeof...(Ns);
        // Replaces size(), which I don't know how to support on both
        // g++ and VS2013.
    };

    template< std::size_t... Ns >
    using index_sequence = integer_sequence< std::size_t, Ns... >;

#endif

  }
}

#endif // INTEGER_SEQUENCE_HPP_
// vi: set ts=2 sts=2 sw=2 et ai: //
